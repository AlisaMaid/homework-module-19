#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "Animal voice\n";
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Cow voice\n";
    }
};

class Chicken : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Chicken voice\n";
    }
};

class Sheep : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Sheep voice\n";
    }
};

int main()
{
    Animal* farm[10];
    for (int i = 0; i < 10; i++)
    {
        switch (i % 3)
        {
        case 0:
            farm[i] = new Cow();
            break;
        case 1:
            farm[i] = new Chicken();
            break;
        case 2:
            farm[i] = new Sheep();
            break;
        }
    }

    for (int i = 0; i < 10; i++)
    {
        farm[i]->Voice();
    }
        

}